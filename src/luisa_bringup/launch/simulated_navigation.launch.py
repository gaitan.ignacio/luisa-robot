from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch_ros.substitutions import FindPackageShare


def generate_launch_description():
    ld = LaunchDescription()
    
    luisa_bringup_package = FindPackageShare('luisa_bringup')
    luisa_description_package = FindPackageShare('luisa_description')
    default_model_path = PathJoinSubstitution([luisa_description_package, 'urdf', 'luisa_robot.urdf.xacro'])
    default_rviz_config_path = PathJoinSubstitution([luisa_description_package, 'rviz', 'urdf.rviz'])

    # These parameters are maintained for backwards compatibility
    model_arg = DeclareLaunchArgument(name='urdf_model_path', default_value=default_model_path,
                                      description='Path to luisa robot urdf file relative to luisa_description package')
    ld.add_action(model_arg)

    gui_arg = DeclareLaunchArgument(name='jsp_gui', default_value='true', choices=['true', 'false'],
                                    description='Flag to enable joint_state_publisher_gui')
    ld.add_action(gui_arg)

    rviz_arg = DeclareLaunchArgument(name='rviz_config', default_value=default_rviz_config_path,
                                     description='Absolute path to rviz config file')
    ld.add_action(rviz_arg)

    ld.add_action(IncludeLaunchDescription(
        PathJoinSubstitution([luisa_bringup_package, 'launch', 'display.launch.py']),
        launch_arguments={
            'luisa_description': 'luisa_description',
            'urdf_model_path': LaunchConfiguration('urdf_model_path'),
            'rviz_config': LaunchConfiguration('rviz_config'),
            'jsp_gui': LaunchConfiguration('jsp_gui')}.items()
    ))

    return ld