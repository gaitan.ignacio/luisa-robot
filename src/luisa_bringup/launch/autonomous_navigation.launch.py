from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()

    robot_names = ["Luisa"]

    luisa_control_nodes = []

    for name in robot_names:
        luisa_control_nodes.append(Node(
            package="luisa_control",
            executable="luisa_control",
            name="luisa_control_" + name.lower(),
            parameters=[{"robot_name": name}]
        ))

    for node in luisa_control_nodes:
        ld.add_action(node)

    node = Node(
        package="luisa_control",
        executable="luisa_control",
        name="luisa_control"
    )

    ld.add_action(node)
    return ld
