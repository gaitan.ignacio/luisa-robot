from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition, UnlessCondition
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution

from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():
    ld = LaunchDescription()

    # Find the bringup and description packages
    luisa_bringup_package = FindPackageShare('luisa_bringup')
    luisa_description_package = FindPackageShare('luisa_description')

    # Declare the default values for the xacro model description, the rviz config.
    default_model_path = PathJoinSubstitution([luisa_description_package, 'urdf', 'luisa_robot.urdf.xacro'])
    default_rviz_config_path = PathJoinSubstitution([luisa_description_package, 'rviz', 'urdf.rviz'])
    launch_description_path = PathJoinSubstitution([luisa_bringup_package, 'launch', 'description.launch.py'])

    # Add launch arguments with their dafault values to let this file be called in default mode
    ld.add_action(DeclareLaunchArgument(name='luisa_description', default_value='luisa_description',
                                        description='The package where the luisa robot description is located'))
    
    ld.add_action(DeclareLaunchArgument(name='urdf_model_path', default_value=default_model_path,
                                        description='The path to the robot model description relative to the package root'))

    ld.add_action(DeclareLaunchArgument(name='jsp_gui', default_value='true', choices=['true', 'false'],
                                        description='Flag to enable joint_state_publisher_gui'))

    ld.add_action(DeclareLaunchArgument(name='rviz_config', default_value=default_rviz_config_path,
                                        description='Absolute path to rviz config file'))

    ld.add_action(IncludeLaunchDescription(
        launch_description_path,
        launch_arguments={
            'luisa_description': LaunchConfiguration('luisa_description'),
            'urdf_model_path': LaunchConfiguration('urdf_model_path')}.items()
    ))

    # Depending on gui parameter, either launch joint_state_publisher or joint_state_publisher_gui
    ld.add_action(Node(
        package='joint_state_publisher',
        executable='joint_state_publisher',
        condition=UnlessCondition(LaunchConfiguration('jsp_gui'))
    ))

    ld.add_action(Node(
        package='joint_state_publisher_gui',
        executable='joint_state_publisher_gui',
        condition=IfCondition(LaunchConfiguration('jsp_gui'))
    ))

    ld.add_action(Node(
        package='rviz2',
        executable='rviz2',
        output='screen',
        arguments=['-d', LaunchConfiguration('rviz_config')],
    ))
    return ld