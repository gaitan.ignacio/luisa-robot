import launch
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()

    eyes_names = ['left_eye', 'right_eye']

    luisa_eyes_nodes = []
    luisa_yolo_nodes = []

    for eye_name in eyes_names:
        luisa_eyes_nodes.append(
            Node(
                package='luisa_perception',
                executable='eyes_node',
                name=eye_name,
                remappings=[("camera/rgb/image_raw", "camera/rgb/image_raw_"+eye_name)],
                parameters=[
                    {"publish_rate": 2.0},
                ]
            )
        )

        luisa_yolo_nodes.append(
            Node(
                package='luisa_perception',
                executable='object_detection_node',
                remappings=[("camera/rgb/image_raw", "camera/rgb/image_raw_"+eye_name),
                            ("camera/rgb/image_raw_output","camera/rgb/image_raw_output_"+eye_name)],
                name='yolo_node'+eye_name
            )
        )

    for node in luisa_eyes_nodes:
        ld.add_action(node)

    for yolo_node in luisa_yolo_nodes:
        ld.add_action(yolo_node)

    return ld