import launch
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='luisa_perception',
            executable='camera_node',
            name='camera_node'
        ),
        Node(
            package='luisa_perception',
            executable='object_detection_node',
            name='object_detection_node'
        ),
        # Add other nodes required for visualization and interaction
    ])
