from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import Command
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution


from launch_ros.actions import Node
from launch_ros.parameter_descriptions import ParameterValue
from launch_ros.substitutions import FindPackageShare


def generate_launch_description():
    ld = LaunchDescription()

    # Find the bringup and description packages
    luisa_bringup_package = FindPackageShare('luisa_bringup')
    luisa_description_package = FindPackageShare('luisa_description')

    # Declare the default values for the xacro model description, the rviz config.
    default_model_path = PathJoinSubstitution([luisa_description_package, 'urdf', 'luisa_robot.urdf.xacro'])

    ld.add_action(DeclareLaunchArgument('luisa_description', default_value='luisa_description',
                                        description='The package where the luisa robot description is located'))

    ld.add_action(DeclareLaunchArgument('urdf_model_path', default_value=default_model_path,
                                        description='The path to the robot model description relative to the package root'))

    robot_description_content = ParameterValue(Command(['xacro ', default_model_path]), value_type=str)

    robot_state_publisher_node = Node(package='robot_state_publisher',
                                      executable='robot_state_publisher',
                                      parameters=[{
                                          'robot_description': robot_description_content,
                                      }])

    ld.add_action(robot_state_publisher_node)
    return ld
