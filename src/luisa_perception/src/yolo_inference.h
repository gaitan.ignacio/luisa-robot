#pragma once

#define RET_OK std::string()

#include <string>
#include <vector>
#include <cstdio>
#include <opencv2/opencv.hpp>
#include "onnxruntime_cxx_api.h"
#include "rclcpp/rclcpp.hpp"
#include <cuda_fp16.h>


enum YoloModelType {
    //FLOAT32 MODEL
    // YOLO_ORIGIN_V5 = 0,
    YOLO_ORIGIN_V8 = 1,
    // YOLO_POSE_V8 = 2,
    // YOLO_CLS_V8 = 3,
    YOLO_ORIGIN_V8_HALF = 4,
    // YOLO_POSE_V8_HALF = 5,
    // YOLO_CLS_V8_HALF = 6
};


struct YoloInitParam {
    YoloModelType modelType = YOLO_ORIGIN_V8;
    std::string modelPath;
    std::vector<int> imgSize = {640, 640};
    float rectConfidenceThreshold = 0.6;
    float iouThreshold = 0.5;
    bool cudaEnable = false;
    int logSeverityLevel = 3;
    int intraOpNumThreads = 1;
};


struct YoloResult {
    int classId;
    float confidence;
    cv::Rect box;
};


class YoloCore {
public:
    YoloCore(rclcpp::Logger logger); // Constructor

    ~YoloCore();

public:
    std::string createSession(YoloInitParam &iParams);
    std::string runSession(cv::Mat &iImg, std::vector<YoloResult> &oResult);
    std::string warmUpSession();
    template<typename N>
    std::string tensorProcess(clock_t &starttime_1, cv::Mat &iImg, N &blob, std::vector<int64_t> &inputNodeDims, std::vector<YoloResult> &oResult);
    std::vector<std::string> classes{};

private:
    Ort::Env env;
    Ort::Session *session;
    bool cudaEnable;
    Ort::RunOptions options;
    std::vector<const char *> inputNodeNames;
    std::vector<const char *> outputNodeNames;
    rclcpp::Logger logger_;
    YoloModelType modelType;
    std::vector<int> imgSize;
    float rectConfidenceThreshold;
    float iouThreshold;
};