#ifndef PERCEPTION_UTILS_H
#define PERCEPTION_UTILS_H

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

class PerceptionUtils
{
public:
    static void convertRosImageToCv(const sensor_msgs::Image::ConstPtr& ros_image_msg, cv::Mat& cv_image);
    static void convertCvImageToRos(const cv::Mat& cv_image, sensor_msgs::Image& ros_image_msg);
};

#endif // PERCEPTION_UTILS_H
