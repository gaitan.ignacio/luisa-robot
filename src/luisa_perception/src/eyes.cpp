#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <image_transport/image_transport.hpp>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <filesystem>

class EyesNode : public rclcpp::Node {
public:
    enum class SourceType {
        Webcam,
        VideoFile
    };

    // Constructor
    EyesNode(SourceType source_type, const std::string& source_path) : Node("eyes"), source_type_(source_type), source_path_(source_path) {
        // Declare a parameter for publishing rate (default: 5 Hz)
        this->declare_parameter("publish_rate", 5.0);
        double publish_rate = this->get_parameter("publish_rate").as_double();

        // Create the image publisher
        image_publisher_ = image_transport::create_publisher(this, "camera/rgb/image_raw");

        // Initialize the video capture based on the source type
        if (source_type_ == SourceType::Webcam) {
            video_capture_.open("/dev/video0");
        } else if (source_type_ == SourceType::VideoFile) {
            video_capture_.open(source_path_);
        }

        // Check if the video capture is successful
        if (!video_capture_.isOpened()) {
            RCLCPP_ERROR(this->get_logger(), "Failed to open the video source.");
            rclcpp::shutdown();
        }

        // Set the publishing timer
        publish_timer_ = this->create_wall_timer(std::chrono::milliseconds(static_cast<int>(1000.0 / publish_rate)),
                                                 std::bind(&EyesNode::publishImage, this));
    }

private:
    // Callback to publish images
    void publishImage() {
        cv::Mat frame;
        video_capture_ >> frame;

        if (!frame.empty()) {
            // Convert the OpenCV image to a ROS2 sensor_msgs::msg::Image message
            auto image_msg = cv_bridge::CvImage(std_msgs::msg::Header(), "bgr8", frame).toImageMsg();
            image_publisher_.publish(image_msg);
        }
    }

    image_transport::Publisher image_publisher_;
    cv::VideoCapture video_capture_;
    rclcpp::TimerBase::SharedPtr publish_timer_;
    EyesNode::SourceType source_type_;
    std::string source_path_;
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);

    // Set the default source type and path (video file by default)
    EyesNode::SourceType source_type = EyesNode::SourceType::VideoFile;
    std::string current_directory = std::filesystem::current_path().string();
    std::string source_path = current_directory + "/test/test_1.mp4";

    // Create and run the EyesNode
    rclcpp::spin(std::make_shared<EyesNode>(source_type, source_path));
    rclcpp::shutdown();
    return 0;
}
