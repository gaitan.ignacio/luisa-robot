#include "perception_utils.h"

void PerceptionUtils::convertRosImageToCv(const sensor_msgs::Image::ConstPtr& ros_image_msg, cv::Mat& cv_image)
{
    try {
        cv_image = cv_bridge::toCvShare(ros_image_msg, "bgr8")->image;
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
    }
}

void PerceptionUtils::convertCvImageToRos(const cv::Mat& cv_image, sensor_msgs::Image& ros_image_msg)
{
    try {
        cv_bridge::CvImage ros_image;
        ros_image.header.stamp = ros::Time::now();
        ros_image.encoding = "bgr8";
        ros_image.image = cv_image;
        ros_image_msg = *ros_image.toImageMsg();
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
    }
}
