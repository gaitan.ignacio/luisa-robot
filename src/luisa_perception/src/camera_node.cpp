// camera_node.cpp

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "camera_node");
    ros::NodeHandle nh;

    // Camera parameters (replace with actual values)
    int camera_width = 640;
    int camera_height = 480;
    int camera_framerate = 30;

    // Create a ROS publisher for the camera image
    ros::Publisher image_pub = nh.advertise<sensor_msgs::Image>("/camera/image_raw", 1);

    // Create a ROS publisher for the camera info (optional)
    ros::Publisher camera_info_pub = nh.advertise<sensor_msgs::CameraInfo>("/camera/camera_info", 1);

    // Initialize OpenCV camera capture (replace with actual camera initialization code)
    cv::VideoCapture cap(0);
    if (!cap.isOpened()) {
        ROS_ERROR("Failed to open the camera!");
        return -1;
    }
    cap.set(cv::CAP_PROP_FRAME_WIDTH, camera_width);
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, camera_height);
    cap.set(cv::CAP_PROP_FPS, camera_framerate);

    ros::Rate loop_rate(camera_framerate);

    while (ros::ok()) {
        // Capture a frame from the camera
        cv::Mat frame;
        cap >> frame;

        if (frame.empty()) {
            ROS_WARN("Empty frame received!");
            continue;
        }

        // Convert the OpenCV frame to a ROS sensor_msgs::Image message
        cv_bridge::CvImage ros_image;
        ros_image.header.stamp = ros::Time::now();
        ros_image.encoding = "bgr8";
        ros_image.image = frame;

        // Publish the image
        image_pub.publish(ros_image.toImageMsg());

        // (Optional) Publish camera info (replace with actual camera info if available)
        sensor_msgs::CameraInfo camera_info_msg;
        camera_info_msg.header.stamp = ros::Time::now();
        camera_info_msg.width = camera_width;
        camera_info_msg.height = camera_height;
        // Fill in other camera_info_msg parameters here if available
        camera_info_pub.publish(camera_info_msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}