#include "rclcpp/rclcpp.hpp"
#include <sensor_msgs/msg/image.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <iomanip>
#include "yolo_inference.h"
#include <filesystem>
#include <fstream>

class YoloNode : public rclcpp::Node {
public:
    YoloNode() : Node("yolo_node"), runOnGPU(true), yoloDetector(new YoloCore(this->get_logger())) {
        current_directory = std::filesystem::current_path().string();
        modelPath = current_directory + "/build/luisa_perception/models/yolov8n.onnx";
        configPath = current_directory + "/build/luisa_perception/config/coco.yaml";
        RCLCPP_INFO(this->get_logger(), "Config directory path: %s", configPath.c_str());
        RCLCPP_INFO(this->get_logger(), "Model directory path: %s", modelPath.c_str());
        // Read coco labels
        this->readCocoLabels(yoloDetector);

        // Initialize the inference session
        params = {YOLO_ORIGIN_V8, modelPath, {640, 640}, 0.1, 0.5, true};  // Initialize it with values
        std::string ret = yoloDetector->createSession(params);
        RCLCPP_INFO(this->get_logger(), "Created session: %s", ret.c_str());


        // Subscribe to the input image topic
        image_subscription = this->create_subscription<sensor_msgs::msg::Image>(
            "camera/rgb/image_raw", 1,
            std::bind(&YoloNode::imageCallback, this, std::placeholders::_1)
        );

        // Create the image publisher
        image_publisher = this->create_publisher<sensor_msgs::msg::Image>("camera/rgb/image_raw_output", 1);
    }
private:
    // Function to read COCO labels from a YAML file
    int readCocoLabels(YoloCore *&yoloDetector) {
        // Open the YAML file
        std::ifstream file(this->configPath);
        if (!file.is_open()) {
            RCLCPP_ERROR(this->get_logger(), "Failed to open file");
            return 1;
        }

        // Read the file line by line
        std::string line;
        std::vector<std::string> lines;
        while (std::getline(file, line)) {
            lines.push_back(line);
        }

        // Find the start and end of the names section
        std::size_t start = 0;
        std::size_t end = 0;
        for (std::size_t i = 0; i < lines.size(); i++) {
            if (lines[i].find("names:") != std::string::npos) {
                start = i + 1;
            } else if (start > 0 && lines[i].find(':') == std::string::npos) {
                end = i;
                break;
            }
        }

        // Extract the names
        std::vector<std::string> names;
        for (std::size_t i = start; i < end; i++) {
            std::stringstream ss(lines[i]);
            std::string name;
            std::getline(ss, name, ':'); // Extract the number before the delimiter
            std::getline(ss, name); // Extract the string after the delimiter
            names.push_back(name);
        }

        yoloDetector->classes = names;
        return 0;
    }
    void imageCallback(const sensor_msgs::msg::Image::SharedPtr msg) {
        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        } catch (cv_bridge::Exception& e) {
            RCLCPP_ERROR(this->get_logger(), "cv_bridge exception: %s", e.what());
            return;
        }

        std::vector<YoloResult> output;
        yoloDetector->runSession(cv_ptr->image, output);

        cv::Scalar redColor = cv::Scalar(0, 0, 255);

        // Draw bounding boxes and class labels
        for (const auto& detection : output) {
            cv::rectangle(cv_ptr->image, detection.box, redColor, 2);

            std::string classString = std::to_string(detection.classId) + ' ' + std::to_string(detection.confidence).substr(0, 4);
            cv::Size textSize = cv::getTextSize(classString, cv::FONT_HERSHEY_DUPLEX, 1, 2, 0);
            cv::Rect textBox(detection.box.x, detection.box.y - 40, textSize.width + 10, textSize.height + 20);

            cv::rectangle(cv_ptr->image, textBox, redColor, cv::FILLED);
            cv::putText(cv_ptr->image, classString, cv::Point(detection.box.x + 5, detection.box.y - 10),
                        cv::FONT_HERSHEY_DUPLEX, 1, cv::Scalar(0, 0, 0), 2, 0);
        }

        // Convert the processed image back to ROS message format
        cv_bridge::CvImage out_msg;
        out_msg.header = msg->header;
        out_msg.encoding = sensor_msgs::image_encodings::BGR8;
        out_msg.image = cv_ptr->image;

        // Convert shared_ptr to unique_ptr
        std::unique_ptr<sensor_msgs::msg::Image> unique_msg = std::make_unique<sensor_msgs::msg::Image>(*out_msg.toImageMsg());

        // Publish the processed image
        image_publisher->publish(std::move(unique_msg));
    }
    bool runOnGPU;
    std::string current_directory;
    std::string modelPath;
    std::string configPath;
    YoloInitParam params;  // Declare the variable
    YoloCore *yoloDetector;
    cv_bridge::CvImagePtr cv_ptr;
    cv::Scalar redColor;
    rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr image_subscription;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr image_publisher;
};

int main(int argc, char **argv) {
    rclcpp::init(argc, argv);
    auto node = std::make_shared<YoloNode>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
