from setuptools import find_packages, setup

package_name = 'luisa_control'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Ignacio Gaitan',
    maintainer_email='igaitan@icloud.com',
    description="Contains nodes dedicated to motion control and planning, enabling precise control of the robot's movements and actions.",
    license='GNU AFFERO GENERAL PUBLIC LICENSE',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "move_arm = luisa_control.move_arm:main",
            "vision_interpretation = luisa_control.vision_interpretation:main"
        ],
    },
)
