#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from sensor_msgs.msg import Image

class VisionInterpretation(Node): # MODIFY NAME
    def __init__(self):
        super().__init__("vision_interpretation") # MODIFY NAME
        self.subscriber_ = self.create_subscription(Image, 'cam_image', self.image_callback, 10)

    def image_callback(self, msg):
        self.get_logger().info(str(len(msg.data)))


def main(args=None):
    rclpy.init(args=args)
    node = VisionInterpretation() # MODIFY NAME
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
