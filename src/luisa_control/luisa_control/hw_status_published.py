#!/usr/bin/env python3

import rospy
from luisa_msgs.msg import DetectedHuman

if __name__ == '__main__':
    rospy.init_node("hw_status_publisher")

    pub = rospy.Publisher("/luisa/hardware_status", DetectedHuman, queue_size=10)

    rate = rospy.Rate(5)

    while not rospy.is_shutdown():
        msg = DetectedHuman()
        msg.temperature = 45
        msg.are_motors_up = True
        msg.debug_message = "Everything is running well."

        pub.publish(msg)

        rate.sleep()