#!/usr/bin/env python3

import rospy
from rospy_tutorials.srv import AddTwoInts


if __name__ == '__main__':
    rospy.init_node("move_arm_client")

    rospy.wait_for_service("/move_arm")

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        try:
            move_arm_client = rospy.ServiceProxy("/move_arm", AddTwoInts)
            response = move_arm_client(2,6)
            rospy.loginfo("SUM is : " + str(response.sum))
        except rospy.ServiceException as e:
            rospy.logwarn("Service failed: " + str(e))

        rate.sleep()

