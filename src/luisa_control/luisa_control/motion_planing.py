#!/usr/bin/env python3

import rospy
from std_msgs.msg import String

if __name__ == '__main__':
    rospy.init_node('motion_planning_node')
    rospy.loginfo('Node started')

    pub = rospy.Publisher('/coordinates', String, queue_size=10)

    rate = rospy.Rate(2)

    while not rospy.is_shutdown():
        rospy.loginfo('Publishing data')
        msg = String()
        msg.data = 'x=0,y=0'
        pub.publish(msg)
        rate.sleep()

    rospy.loginfo('Node stopped')
