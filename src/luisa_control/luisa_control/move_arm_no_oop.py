#! /usr/bin/env python3

import rospy
from rospy_tutorials.srv import AddTwoInts

def move_arm(req):
    result = req.a + req.b
    rospy.loginfo("Sum of " + str(req.a) + " and " + str(req.b) + " is " + str(result))
    return result

if __name__ == '__main__':
    rospy.init_node("move_arm")

    rospy.loginfo("Moving arm")

    service = rospy.Service("/move_arm", AddTwoInts, move_arm)
    rospy.loginfo("Service has been started")
    rospy.spin()
