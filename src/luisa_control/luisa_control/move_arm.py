#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from example_interfaces.msg import String

class MoveArm(Node):
    def __init__(self):
        super().__init__('move_arm')
        self.counter_ = 0
        self.get_logger().info("Moving arm")
        self.create_timer(0.5, self.timer_callback)

        self.publisher_ = self.create_publisher(String, "arm_position", 10)
        self.timer_ = self.create_timer(0.5, self.publish_news)
        self.get_logger().info("Robot moving arm has been started")

    def publish_news(self):
        msg = String()
        msg.data = "x=0, y=1"
        self.publisher_.publish(msg=msg)


    def timer_callback(self):
        self.counter_ += 1
        self.get_logger().info("Hello" + str(self.counter_))

def main(args=None):
    rclpy.init(args=args)
    node = MoveArm()
    rclpy.spin(node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()