#include <ros/ros.h>
#include <rospy_tutorials/AddTwoInts.h>

bool say_something(rospy_tutorials::AddTwoInts::Request &req,
		rospy_tutorials::AddTwoInts::Response &res)
{
	int result = req.a + req.b;
	ROS_INFO("%d + %d = %d", (int)req.a, (int)req.b, (int)result);
	res.sum = result;
	return true;
}

int main (int argc, char **argv)
{
	ros::init(argc, argv, "say_something");
	ros::NodeHandle nh;

	ros::ServiceServer server = nh.advertiseService("/say_something", say_something);

	ros::spin();
}
