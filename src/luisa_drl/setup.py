from setuptools import find_packages, setup

package_name = 'luisa_drl'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Ignacio Gaitan',
    maintainer_email='igaitan@icloud.com',
    description="Incorporates Deep Reinforcement Learning (DRL) techniques, harnessing AI algorithms to enhance the robot's decision-making and learning capabilities for complex tasks.",
    license='GNU AFFERO GENERAL PUBLIC LICENSE',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
