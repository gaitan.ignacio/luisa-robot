#include "rclcpp/rclcpp.hpp"

class MoveMotor: public rclcpp::Node
{
public:
    MoveMotor(): Node("move_motor"), counter_(0)
    {
        RCLCPP_INFO(this->get_logger(), "Hello from class");

        timer_ = this->create_wall_timer(std::chrono::seconds(1),
                                        std::bind(&MoveMotor::timerCallback, this));
    }

private:
    void timerCallback()
    {   
        counter_ ++;
        RCLCPP_INFO(this->get_logger(), "Hello %d", counter_);
    }
    rclcpp::TimerBase::SharedPtr timer_;
    int counter_;
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<MoveMotor>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}