#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/msg/string.hpp"

class MoveShoulder : public rclcpp::Node
{
public:
    MoveShoulder() : Node("move_shoulder")
    {
        subscriber_ = this->create_subscription<example_interfaces::msg::String>("arm_position", 10, std::bind(&MoveShoulder::callbackArmPosition, this, std::placeholders::_1));

        RCLCPP_INFO(this->get_logger(), "MoveShoulder has been started.");
    }

private:
    void callbackArmPosition(const example_interfaces::msg::String::SharedPtr msg)
    {
        RCLCPP_INFO(this->get_logger(), "%s", msg->data.c_str());
    }
    rclcpp::Subscription<example_interfaces::msg::String>::SharedPtr subscriber_;
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<MoveShoulder>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
