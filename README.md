# Luisa ROS2 Robotics Project

<p align="center">
  <img src="assets/image.png" alt="Luisa Robot">
</p>

Welcome to the Luisa ROS2 Robotics Project repository! This project aims to develop a complete robotics system for the "Luisa" robot, encompassing control, perception, interaction, and more, using the Robot Operating System (ROS2).

## Table of Contents
- [Introduction](#introduction)
- [Bill of Materials](#bill-of-materials)
- [Installation](#installation)
  - [Dockerized Installation](#dockerized-installation)
  - [Local Installation](#local-installation)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [License](#license)

## Introduction
The Luisa robot is an advanced humanoid robot equipped with various hardware components and capable of performing various tasks. This repository contains RO2 packages that provide the software infrastructure to control the robot, perform perception tasks, interact with humans, and more.

## Bill of Materials
- [BOM (Bill of Materials)](./docs/BOM.md)

## Installation
The "Installation" section provides a guide for setting up the Luisa ROS2 Robotics Project on your machine. There are two installation types covered in this section: "Dockerized Installation" for running the project within a Docker container and "Local Installation" for setting up and running the project directly on your local machine. These steps are essential to prepare your environment for the project, ensuring a smooth and tailored setup experience for your chosen deployment method.

### Dockerized Installation

"Dockerized Installation" offers instructions for running the project within a Docker container, providing a reproducible and isolated environment.

Running the following you will be easily accessing the [setup.sh](scripts/setup.sh) which holds all the necessary installation steps to prepare the host machine for dockerized installation.

```
make setup
```

After running all the necessary installations in the host machine you should be able to easily build the luisa-robot image with the following command:

```
make build_image
```

### Local Installation

"Local Installation" focuses on setting up and running the project directly on your local machine, without the use of containerization.

1. [Install ROS](https://docs.ros.org/en/humble/Installation.html): Ensure you have Humble Hawksbill installed on your Ubuntu Linux 22.04.
2. [Install OnnxRuntime](https://onnxruntime.ai/docs/install/): To install `onnxruntime-linux-x64-gpu-1.15.1`, follow the steps below:

```
wget https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-x64-gpu-1.15.1.tgz

tar -xzf onnxruntime-linux-x64-gpu-1.15.1.tgz

sudo mv onnxruntime-linux-x64-gpu-1.15.1 /usr/local/lib

sudo ldconfig

echo 'export LD_LIBRARY_PATH=/usr/local/lib/onnxruntime-linux-x64-gpu-1.15.1/lib:$LD_LIBRARY_PATH' >> ~/.bashrc
```

3. Clone this repository:
```
git clone https://gitlab.com/gaitan.ignacio/luisa-robot.git
```
4. Build the ROS packages:
```
cd luisa-ros
make build
```
5. Source the workspace:
```
source install/setup.bash
```

## Usage
1. **Launching the Robot System**:

Launch the robot system using the provided launch files. Adjust the arguments as needed for your robot setup.
```
make launch
```

2. **Teleoperation**:

If applicable, use the teleoperation node to control the robot using a joystick or another input method.
```
ros2 run luisa_control teleoperation_node
```

## Documentation
For additional documentation, including the robot's physical description, [software architecture](docs/SOFTWARE_ARCHITECTURE.md), and how to use specific ROS packages, refer to the [Docs](docs/) directory.

## Contributing
I welcome contributions to enhance the "Luisa" ROS2 Robotics Project! If you'd like to contribute, please follow the guidelines outlined in [CONTRIBUTING.md](CONTRIBUTING.md).

## License
This project is licensed under the [GNU AGPL License](LICENSE).

---

For questions or feedback, please contact [Ignacio Gaitán](mailto:igaitan@icloud.com).