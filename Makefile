# Set the paths to your ROS2 workspace and source directory
SRC_DIR ?= $(PWD)/src/*

# Default target
all: launch

# export DISPLAY=localhost:0.0
setup:
	./scripts/setup.sh

# Build the ROS2 workspace
build:
	colcon build --paths $(SRC_DIR)

build_image:
	docker build . -t luisa-robot

launch_image:
	docker run --gpus all -it luisa-robot

# Clean the build and install artifacts
clean:
	rm -rf $(PWD)/install && rm -rf $(PWD)/build && rm -rf $(PWD)/log

# Run tests for your ROS2 packages
test:
	colcon test --paths $(SRC_DIR)

# Rebuild the workspace (clean and build)
rebuild: clean build

# Install the built packages into the ROS2 install space
install:
	colcon build --paths $(SRC_DIR) --symlink-install

# Launch the luisa_brungup launch file
launch:
	ros2 launch luisa_bringup interactive_interaction.launch.py

# Help target to display available targets
help:
	@echo "ROS2 Workspace Makefile Targets:"
	@echo "  make all (default)  : Build the ROS2 workspace"
	@echo "  make setup          : Set up the host environment"
	@echo "  make build          : Build the ROS2 workspace"
	@echo "  make clean          : Clean the build and install artifacts"
	@echo "  make test           : Run tests for your ROS2 packages"
	@echo "  make rebuild        : Rebuild the workspace (clean and build)"
	@echo "  make install        : Install the built packages into the ROS2 install space"
	@echo "  make launch         : Run interactive interaction ROS2 launch file"
	@echo "  make help           : Show this help message"

.PHONY: all build clean test rebuild install run help
