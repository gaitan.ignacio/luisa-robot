# Use the "ros" base image with the "humble-ros-base-jammy"
# https://github.com/osrf/docker_images/blob/20e3ba685bb353a3c00be9ba01c1b7a6823c9472/ros/humble/ubuntu/jammy/ros-base/Dockerfile 
FROM ros:humble-ros-base-jammy

# Update and upgrade packages
RUN sudo apt update && sudo apt-get -y upgrade

# Set environment variables for noninteractive mode
ENV TZ=US \
    DEBIAN_FRONTEND=noninteractive

# Configure debconf for noninteractive frontend
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install required CUDA Toolkit 11.8
# https://onnxruntime.ai/docs/execution-providers/CUDA-ExecutionProvider.html
# https://developer.nvidia.com/cuda-11-8-0-download-archive
# https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html
RUN sudo apt-get install -y wget \
    && wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.0-1_all.deb \
    && sudo dpkg -i cuda-keyring_1.0-1_all.deb \
    && sudo apt-get update \
    && sudo apt-get -y install cuda-11-8 \
    && rm cuda-keyring_1.0-1_all.deb

# Download and install ONNX Runtime 1.15
# https://onnxruntime.ai/docs/execution-providers/CUDA-ExecutionProvider.html
RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-x64-gpu-1.15.1.tgz \
    && tar -xzf onnxruntime-linux-x64-gpu-1.15.1.tgz \
    && sudo mv onnxruntime-linux-x64-gpu-1.15.1 /usr/local/lib \
    && sudo ldconfig \
    && rm onnxruntime-linux-x64-gpu-1.15.1.tgz

# Install ROS packages and other dependencies
RUN sudo apt-get update \
    && sudo apt-get -y install ros-humble-example-interfaces \
    ros-humble-image-transport \
    ros-humble-cv-bridge \
    libcanberra-gtk-module \
    libcanberra-gtk3-module

# Install Python 3 pip and specific version of setuptools to prevent warning
# https://answers.ros.org/question/396439/setuptoolsdeprecationwarning-setuppy-install-is-deprecated-use-build-and-pip-and-other-standards-based-tools/
RUN sudo apt-get -y install python3-pip \
    && pip3 install setuptools==58.2.0 \
    && python3 -m pip install cmake

# Downloaded from https://developer.nvidia.com/rdp/cudnn-download
COPY ./cuda/cudnn-local-repo-ubuntu2204-8.9.6.50_1.0-1_amd64.deb .

## Install cudnn
## https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html
RUN sudo dpkg -i cudnn-local-repo-ubuntu2204-8.9.6.50_1.0-1_amd64.deb \
    && sudo cp /var/cudnn-local-repo-*/cudnn-local-*-keyring.gpg /usr/share/keyrings/ \
    && sudo apt-get update \
    && sudo apt-get -y install libcudnn8=8.9.6.50-1+cuda11.8 \
    libcudnn8-dev=8.9.6.50-1+cuda11.8 \
    libcudnn8-samples=8.9.6.50-1+cuda11.8

# Add environment setup to .bashrc for interactive shells
RUN echo 'export PATH=/usr/local/cuda/bin:$PATH' >> ~/.bashrc
RUN echo 'source /opt/ros/humble/setup.bash' >> ~/.bashrc
RUN echo 'source /app/install/setup.bash' >> ~/.bashrc
RUN echo 'source /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash' >> ~/.bashrc
RUN echo 'export LD_LIBRARY_PATH=/usr/local/lib/onnxruntime-linux-x64-gpu-1.15.1/lib:$LD_LIBRARY_PATH' >> ~/.bashrc

WORKDIR /app

COPY . .

RUN make build
RUN chmod +x /app/entrypoint.sh
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["launch"]