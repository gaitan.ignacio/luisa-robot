# Change Log

All notable changes to the "Luisa" ROS Robotics Project will be documented in this file.

## [Unreleased]
### Added
- WIP

## [0.1.0] - 2023-08-20

### Added
- Introduced the inaugural version of the "Luisa" robot.
- Established fundamental boilerplate.
- Established a straightforward folder structure pipeline.
- Made the initial commit to kickstart the project.

[Unreleased]: https://gitlab.com/gaitan.ignacio/luisa-robot/compare/0.1.0...HEAD
[0.1.0]: https://gitlab.com/gaitan.ignacio/luisa-robot/-/releases/0.1.0