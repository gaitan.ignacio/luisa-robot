#!/bin/bash
set -e

# Source files
source /opt/ros/humble/setup.bash;
source /app/install/setup.bash;

# Export env variables
export PATH=/usr/local/cuda/bin:$PATH;
export LD_LIBRARY_PATH=/usr/local/lib/onnxruntime-linux-x64-gpu-1.15.1/lib:$LD_LIBRARY_PATH;

# Define an array of valid commands
valid_commands=("setup" "build" "clean" "test" "rebuild" "install" "launch")

# Default command if not provided or invalid
default_command="launch"

# Check if a command was provided as an argument
if [ $# -gt 0 ]; then
    TARGET="$1"
else
    TARGET="$default_command"
fi

# Check if the provided command is in the list of valid commands
if [[ " ${valid_commands[*]} " =~ " $TARGET " ]]; then
    make $TARGET
else
    echo "Invalid command: $TARGET. Defaulting to $default_command."
    make $default_command
fi

# End of entrypoint.sh
