# Luisa ROS2 Robotics Project - Developer Guide

Welcome to the Developer Guide for the Luisa ROS2 Robotics Project. This guide provides essential information for developers who are contributing to or working with this project.

## Table of Contents

- [Version Control and Branching](#version-control-and-branching)
- [Coding Standards](#coding-standards)
- [ROS2 Best Practices](#ros2-best-practices)
- [URDF and Gazebo Guidelines](#urdf-and-gazebo-guidelines)
- [C++ and Python Coding Practices](#c-and-python-coding-practices)
- [Testing](#testing)
- [Documentation](#documentation)

## Version Control and Branching

- Use descriptive branch names that convey the purpose of your changes.
- Commit messages should be clear, concise, and follow [best practices](https://chris.beams.io/posts/git-commit/).
- Before creating a new feature or fixing a bug, create a branch from the `main` branch.
- Regularly pull changes from the `main` branch to keep your branch updated.

## Coding Standards

Maintaining a consistent coding style is essential for a collaborative and readable codebase. While our project currently doesn't have a dedicated coding style guide, we encourage contributors to follow some general principles for code readability and consistency. Pay attention to factors like clear variable naming, consistent indentation, and adherence to language-specific best practices. As the project evolves, we may establish more formal coding standards to enhance code quality and collaboration. Your attention to readability and consistency is greatly appreciated.

## ROS2 Best Practices

- Follow ROS2 [best practices](https://docs.ros.org/en/humble/The-ROS2-Project/Contributing/Code-Style-Language-Versions.html).
- Use ROS2 tools for package management, building, and dependency handling.
- Design modular and reusable ROS2 nodes.

## URDF and Gazebo Guidelines

- Keep URDF models and Gazebo configurations modular and well-organized.
- Test URDF models in Gazebo to ensure accurate representations.
- Leverage Gazebo plugins for advanced simulations.

## C++ and Python Coding Practices

- For C++ code, follow the [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html).
- For Python code, adhere to [PEP 8](https://www.python.org/dev/peps/pep-0008/).

## Testing

- Write unit tests for critical components using ROS2 testing frameworks.
- Include simulation tests for Gazebo-based functionalities.
- Regularly run tests to ensure compatibility with different platforms.

## Documentation

- Maintain up-to-date and comprehensive documentation.
- Include usage instructions, architecture diagrams, and API references.
- Consider contributing to improving existing documentation.

Thank you for contributing to the Luisa ROS2 Robotics Project! If you have questions or need assistance, contact [Ignacio Gaitán](mailto:igaitan@icloud.com).
