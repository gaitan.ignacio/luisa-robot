# Structured Modular Architecture for Luisa Robot Software

Luisa Robot Software is a personal ROS2 project aimed at providing entertainment during leisure time, while also serving as a platform for developing new skills in AI and robotics technologies. The project draws inspiration from the open-source project [InMoov](https://inmoov.fr) by utilizing its 3D printed components for the robot's skeleton. However, the codebase is entirely original and developed independently with state of the art technologies.

The initial framework leverages InMoov's 3D printed parts for rapid real world testing, but the project is not confined to this foundation and is anticipated to undergo its own structural evolution in future releases.

The packages included or planned for inclusion are described below. The primary hardware additions that I am initially planning to implement include two cameras for world image perception, as well as a speech synthesis module for human interaction.

## 1. Packages

The organization and structure of the Luisa Robot Software project are facilitated through a collection of packages. Each package encapsulates a specific set of functionalities and resources that contribute to the overall operation of the robot. These packages provide a modular approach to software development, allowing distinct aspects of the robot's capabilities, such as control, perception, interaction, and simulation, to be developed, tested, and managed independently. This modular design not only promotes code reusability but also simplifies maintenance and enhances collaboration among developers. In the following sections, each package's purpose, contents, and contributions to the project's functionality are elaborated upon.

- **luisa_bringup**: Responsible for initializing and configuring the robot's hardware and software components for proper operation.
- **luisa_interfaces**: Defines the custom message types, service types, and action types used for communication between different parts of the robot's software architecture.
- **luisa_description**: Encompasses the URDF (Unified Robot Description Format) and mesh files, providing a comprehensive representation of the robot's physical structure in the virtual world.
- **luisa_simulation**: Integrates RViz and Gazebo to simulate and visualize the robot's behaviors, sensor data, and interactions in a dynamic 3D environment for effective testing and validation.
- **luisa_contro**l: Contains nodes dedicated to motion control and planning, enabling precise control of the robot's movements and actions.
- **luisa_drl**: Incorporates Deep Reinforcement Learning (DRL) techniques, harnessing AI algorithms to enhance the robot's decision-making and learning capabilities for complex tasks.
- **luisa_interaction**: Hosts nodes that enable natural and intuitive interaction between humans and the robot, fostering seamless communication and collaboration.
- **luisa_perception**: Houses nodes responsible for perception tasks, granting the robot the ability to understand and interpret its surrounding environment through sensor data.
- **luisa_hardware**: Comprises the essential driver nodes essential for managing and controlling the various hardware components that constitute the robot, ensuring their proper functioning and integration within the system.

## 2. Nodes
Nodes are modular units of the robot's software architecture that perform specific tasks. Each node handles a distinct aspect of the robot's operation, such as motion control, perception, interaction, or planning. They communicate with each other through topics, services, and actions, contributing to the overall functionality of the robot.

- **neck_controller**: Controls neck joints using ROS control messages.
- **camera_node**: Captures camera images and publishes them.
- **object_detection_node**: Performs object detection on camera images.
- **speech_synthesis_node**: Synthesizes speech from text and plays it.
- **motion_planning_node**: Generates smooth, collision-free motion paths.
- **navigation_node**: Handles autonomous navigation and pathfinding.
- **grasping_control_node**: Coordinates robot's grasping movements.
- **localization_node**: Estimates robot's position within the environment.
- **human_detection_node**: Detects and tracks humans using computer vision.
- **path_following_node**: Guides robot along predefined paths.
- **speech_recognition_node**: Converts audio inputs into text commands.
- **map_building_node**: Constructs a map of the environment.

## 3. Topics

Topics are communication channels that facilitate data exchange between nodes. They enable real-time information sharing by allowing nodes to publish and subscribe to specific data streams. Topics play a crucial role in conveying sensor data, commands, and other information throughout the robot's software ecosystem.

- **/luisa/neck_joint_states**: Publishes the joint states of the robot arm.
- **/luisa/camera/image_raw**: Publishes raw images from the camera.
- **/luisa/interaction/speech_synthesis**: Publishes synthesized speech.
- **/luisa/interaction/user_commands**: Listens to commands or instructions from users for the robot to execute.
- **/luisa/navigation/goal**: Receives navigation goals for the robot to reach specific locations within the environment.
- **/luisa/navigation/path**: Publishes planned navigation paths for the robot to follow.
- **/luisa/grasping/target_object**: Communicates the target object for the robot's grasping action.
- **/luisa/localization/estimated_pose**: Publishes the estimated pose (position and orientation) of the robot within the map.
- **/luisa/human_detection/detected_people**: Provides information about detected humans, including their positions and features.
- **/luisa/speech_recognition/recognized_speech**: Publishes recognized speech commands converted from audio inputs.
- **/luisa/simulation/control_command**: Sends control commands to simulate the robot's actions in the simulation environment.
- **/luisa/mapping/environment_map**: Publishes the constructed map of the environment for navigation and planning.
- **/luisa/status/robot_state**: Broadcasts the current state or mode of the robot, such as idle, moving, interacting, etc.
- **/luisa/perception/processed_image**: Provides processed images from perception tasks, such as filtered or annotated visual data.
- **/luisa/perception/object_detection**: Publishes object detection results.
- **/luisa/control/motion_commands**: Receives motion commands for controlling the robot's movements, such as velocity and position commands.

## 4. Custom Messages
Messages define the structure and content of the data that nodes exchange through topics. They provide a standardized format for transmitting information, ensuring consistent communication between nodes. Messages can represent various types of data, such as sensor readings, control commands, or status updates.

- **TargetObject.msg**: Describes a detected object for grasping.
- **DetectedHuman.msg**: Provides information about a detected human.
- **NavigationPath.msg**: Represents a planned navigation path for the robot.
- **ObjectDetectionResult.msg**: Contains object detection results.

## 5. Custom Services

Services enable nodes to request specific actions or data from other nodes. They establish a client-server interaction model, where a node (client) sends a request to another node (server), which then processes the request and sends back a response. Services facilitate more complex and stateful interactions between nodes.

- **GraspingControl.srv**: Allows requesting the robot to grasp a specific object.
- **GoToLocation.srv**: Requests the robot to navigate to a specified location.
- **SynthesizeSpeech.srv**: Requests the robot to synthesize speech from text.
- **ObjectDetection.srv**: Requests the robot to perform object detection on an image.

## 6. Configuration Files
Configuration files contain settings and parameters that define the behavior and properties of different components within the robot's software. These files allow for customization and tuning of various aspects, such as sensor calibration, motion parameters, and algorithm parameters. Configuration files play a pivotal role in tailoring the robot's behavior to specific tasks or environments.

- **luisa_control/config/joint_limits.yaml**: Contains joint limits for the arm.
- **luisa_perception/config/object_detection_params.yaml**: Contains parameters for object detection.

## 7. Launch Files
Launch files simplify the process of starting and configuring multiple nodes simultaneously. They define the nodes to be executed, their interconnections, and any required parameters. Launch files streamline the setup and initialization of the robot's software stack, making it easier to deploy and manage different configurations and scenarios.

- **simulated_navigation.launch.py**: Launches the simulated navigation stack for the robot within Gazebo.
- **object_detection_demo.launch.py**: Demonstrates the object detection capabilities of the robot.
- **autonomous_navigation.launch.py**: Sets up the robot for autonomous navigation in a mapped environment.
- **interactive_interaction.launch.py**: Enables interactive human-robot interaction capabilities.
- **grasping_demo.launch.py**: Demonstrates the robot's grasping and manipulation skills.
- **mapping_navigation.launch.py**: Combines mapping and navigation functionalities for exploration.
- **user_defined_behavior.launch.py**: Launches a user-defined sequence of behaviors and actions.
- **full_system_simulation.launch.py**: Simulates the complete robot system with various functionalities.

With this meticulously organized structure, the objective is to partition the functionalities of the Luisa Robot into distinct packages and nodes, establishing a modular framework that enhances maintainability and scalability. The hardware package assumes the responsibility of managing low-level motor control, while the control package orchestrates precise motion planning. Meanwhile, the perception package adeptly manages object detection, and the interaction package excels in synthesizing speech and enabling harmonious human-robot interaction. The seamless flow of information between components is facilitated through meticulously designed topics and services, ensuring cohesive communication within the robot's software ecosystem.