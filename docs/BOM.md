# Bill of Materials

### Servos
| Item | Quantity | Price | Buy Link |
|------|----------|-------|---------|
| HobbyKing HK-15298B | 3 | $19.99 each | [Link](https://hobbyking.com/en_us/hobbykingtm-coreless-digital-hv-mg-bb-servo-11-5kg-0-17sec-66-4g.html) |
| Hitec HS-805BB | 2 | $41.86 each | [Link](https://www.hiteccs.com/actuators/product-details/HS-805BB) |
| Corona DS929HV | 2 | $7.25 each | [Link](https://hobbyking.com/en_us/corona-ds329hv-digital-metal-gear-servo-4-5kg-0-09sec-32g.html) |



### Cameras

| Item | Quantity | Price | Buy Link |
|------|----------|-------|---------|
| Microsoft LifeCam HD-3000 | 2 | $23.67 | [Link](https://a.co/d/1XxFwan)

### Speakers

| Item | Quantity | Price | Buy Link |
|------|----------|-------|---------|
| GHXAPM 2PC 2 inch 4OHM 12W | 1 | $12.90 | [Link](https://www.aliexpress.us/item/2251832632824661.html?spm=a2g0n.orderlist-amp.item.32819139413&aff_trace_key=24554e281ed04049952a936f0dd87e17-1556985676726-09677-UneMJZVf&aff_platform=msite&m_page_id=8029amp-2AezNnBp-osCZ0Hqvulu-A1557026536128&gatewayAdapt=glo2usa4itemAdapt)

### Cables
| Item | Quantity | Price | Buy Link |
|------|----------|-------|---------|
| Grey Flat Ribbon Cable 1.27mm | 20 ft | $2.19 per ft | [Link](https://www.ebay.co.uk/itm/Grey-Flat-Ribbon-Cable-1-27mm-10-14-16-20-26-34-40-Way-/263085715366) |

### Screws
| Item | Quantity | Price | Buy Link |
|------|----------|-------|---------|
| Allen countersunk M3x20MM | 50 | $0.1 each | [Link](screw_size_y_link) |
| Allen countersunk M4x20MM | 50 | $0.1 each | [Link](screw_size_y_link) |
| Allen countersunk M8x100MM | 15 | $0.1 each | [Link](screw_size_y_link) |
| Phillips Flat-Head Wood M3x12MM | 100 | $0.1 each | [Link](screw_size_y_link) |
| Phillips Flat-Head Wood M4x20MM | 50 | $0.1 each | [Link](screw_size_y_link) |


### 3D Printed Parts
| Group | Sub-Group | Item | Color | Quantity | Price | Download Link |
|-------|-------|------|-------|----------|-------|--------------|
| Head  | Eyes  | Ball Support | White | 1 | Free | [STL Left Ball Support Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_ball_support_left_v1.stl) / [STL Right Ball Support Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_ball_support_right_v1.stl) |
|       |       | Balls | White | 1 | Free | [STL Balls Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_balls_v1.stl) |
|       |       | Hinge Curve | White | 1 | Free | [STL Hinge Curve Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_hinge_curve_v1.stl) |
|       |       | Hinges | White | 1 | Free | [STL Hinges Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_hinges_v1.stl) |
|       |       | Holder | White | 1 | Free | [STL Holder Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_holder_v1.stl) |
|       |       | Iris | SkyBlue | 1 | Free | [STL Iris Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_iris_v1.stl) |
|       |       | Nose Connection | White | 1 | Free | [STL Nose Connection Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_nose_connection_v1.stl) |
|       |       | Plate | White | 1 | Free | [STL Left Plate Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_plate_left_v1.stl]) / [STL Right Plate Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_plate_right_v1.stll) |
|       |       | Pupils | Black | 1 | Free | [STL Pupils Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_pupils_v1.stl) |
|       |       | Sclera | Black | 1 | Free | [STL Sclera Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_sclera_v1.stl) |
|       |       | Support | White | 1 | Free | [STL Support Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/eyes/head_eyes_support_v1.stl) |
|       | Face  | Eye Glass  | White | 1 | Free | [STL Eye Glass Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/face/head_face_eye_glass_v1.stl) |
|       |       | Side Hear  | White | 1 | Free | [STL Side Hear Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/face/head_face_side_hear_v1.stl) |
|       |       | Top Mouth  | White | 1 | Free | [STL Top Mouth Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/face/head_face_top_mouth_v1.stl) |
|       | Jaw   | Hinge | White | 1 | Free | [STL Hinge Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/jaw/head_jaw_hinge_v1.stl) |
|       |       | Piston | White | 1 | Free | [STL Piston Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/jaw/head_jaw_piston_v1.stl) |
|       |       | Support Feet | White | 1 | Free | [STL Support Feet Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/jaw/head_jaw_support_feet_v1.stl) |
|       |       | Support | White | 1 | Free | [STL Support Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/jaw/head_jaw_support_V1.stl) |
|       |       | Jaw | White | 1 | Free | [STL Jaw Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/jaw/head_jaw_v1.stl) |
|       | Neck  | Face Holder | White | 1 | Free | [STL Face Holder Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_face_holder_v1.stl) |
|       |       | Gear Holder | White | 1 | Free | [STL Gear Holder Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_gear_holder_v1.stl) |
|       |       | Joint Lower | White | 1 | Free | [STL Joint Lower Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_joint_lower_v1.stl) |
|       |       | Main Gear | White | 1 | Free | [STL Main Gear Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_main_gear_v1.stl) |
|       |       | Main | White | 1 | Free | [STL Main Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_main_v1.stl) |
|       |       | Neck Bolts | White | 1 | Free | [STL Neck Bolts Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_neck_bolts_v1.stl) |
|       |       | Piston Base Front | White | 1 | Free | [STL Piston Base Front Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_piston_base_front_v1.stl) |
|       |       | Piston Base Side | White | 1 | Free | [STL Piston Base Side Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_piston_base_side_v1.stl) |
|       |       | Piston Front | White | 1 | Free | [STL Piston Front Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_piston_front_v1.stl) |
|       |       | Piston Side | White | 1 | Free | [STL Piston Side Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_piston_side_v1.stl) |
|       |       | Plate High | White | 1 | Free | [STL Plate High Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_plate_high_v1.stl) |
|       |       | Plate | White | 1 | Free | [STL Plate Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_plate_v1.stl) |
|       |       | Ring | White | 1 | Free | [STL Ring Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_ring_v1.stl) |
|       |       | Servo Gear | White | 1 | Free | [STL Servo Gear Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_servo_gear_v1.stl) |
|       |       | Servo Holder | White | 1 | Free | [STL Servo Holder Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_servo_holder_v1.stl) |
|       |       | Servo Pivot | White | 1 | Free | [STL Servo Pivot Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_servo_pivot_v1.stl) |
|       |       | Skull Servo Fix | White | 1 | Free | [STL Skull Servo Fix Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_skull_servo_fix_v1.stl) |
|       |       | Speaker Mouth Holder | White | 1 | Free | [STL Speaker Mouth Holder Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_speaker_mouth_holder_v1.stl) |
|       |       | Throat Holder | White | 1 | Free | [STL Throat Holder Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_throat_holder_v1.stl) |
|       |       | Throat Hole | White | 1 | Free | [STL Throat Hole Link](https://gitlab.com/gaitan.ignacio/luisa-robot/raw/main/src/luisa_description/meshes/head/neck/head_neck_throat_hole_v1.stl) |

### Other Hardware
| Item | Quantity | Price | Buy Link |
|------|----------|-------|---------|
| NVIDIA Jetson Orin Nano Developer Kit | 1 | $499 each | [Link](https://www.amazon.com/dp/B0BZJTQ5YP?_encoding=UTF8&psc=1&ref_=cm_sw_r_cp_ud_dp_HKTJYB5F7P79H0KN4HGN) |
| PCA9685 | 1 | $ each | [Link](https://www.amazon.com/HiLetgo-PCA9685-Channel-12-Bit-Arduino/dp/B07BRS249H/ref=sr_1_3?crid=3QYTRR6VCBAQK&keywords=PCA9685&qid=1700361932&sprefix=pca9685%2Caps%2C865&sr=8-3) |