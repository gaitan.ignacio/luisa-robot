# Install required CUDA Toolkit 11.8
# https://developer.nvidia.com/cuda-11-8-0-download-archive
sudo apt-get install -y wget
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.0-1_all.deb
sudo dpkg -i cuda-keyring_1.0-1_all.deb
sudo apt-get update
sudo apt-get -y install cuda-11-8
rm cuda-keyring_1.0-1_all.deb

export PATH=/usr/local/cuda/bin:$PATH

nvcc --version

## Install Docker Engine on Ubuntu
# https://docs.docker.com/engine/install/ubuntu/
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Verify
# sudo docker run hello-world

# Linux post-installation steps for Docker Engine
# https://docs.docker.com/engine/install/linux-postinstall/

sudo groupadd docker

sudo usermod -aG docker $USER

newgrp docker

# Verify
# docker run hello-world

# Access an NVIDIA GPU
# https://docs.docker.com/config/containers/resource_constraints/
sudo apt-get install nvidia-container-runtime

sudo systemctl restart docker

# Colcon
# sudo apt install python3-colcon-common-extensions