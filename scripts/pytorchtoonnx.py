# Models can be downloaded from https://github.com/ultralytics/ultralytics
from ultralytics import YOLO

# Load a YOLOv8 model
model = YOLO("../src/luisa_perception/models/yolov8n.pt")
# Print model information
model.info(verbose=False)  
# Export the model to onnx using opset 12
model.export(format="onnx", opset=12, simplify=True, dynamic=False, imgsz=640)

# Alternatively, you can use the following command for exporting the model in the terminal
# yolo export model=yolov8n.pt opset=12 simplify=True dynamic=False format=onnx imgsz=640,640