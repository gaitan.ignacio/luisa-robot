# Contributing to the Luisa ROS2 Robotics Project

Thank you for considering contributing to the Luisa ROS2 Robotics Project! I welcome contributions from the community to make my project even better. By participating, you help me build a more capable and versatile robot software platform.

## Contributor Agreement

By submitting a contribution to this project, I agree to the following terms:

1. I grant Ignacio Gaitán a perpetual, worldwide, non-exclusive, royalty-free license to use, modify, distribute, and sublicense the contribution.

2. I acknowledge that attribution is important to Ignacio Gaitán, and I agree to include the following notice in any distribution or use of significant portions of the contribution:

[luisa-robot](https://gitlab.com/gaitan.ignacio/luisa-robot) Ignacio Gaitán, 2023.

## How Can I Contribute?

There are several ways to contribute to the Luisa ROS Robotics Project:

1. **Report Issues**: If you encounter any bugs, problems, or have suggestions for improvement, please [open an issue](https://gitlab.com/gaitan.ignacio/luisa-robot/issues). Describe the problem you encountered or the feature you'd like to see, and we'll look into it.

2. **Fix Bugs**: If you have identified a bug in the codebase, feel free to fix it and submit a merge request. Ensure your changes follow our coding guidelines.

3. **Add New Features**: If you have a new feature in mind that would benefit the project, you can implement it and submit a merge request.

4. **Improve Documentation**: I value good documentation and don't always have time to write good docs. If you find any part of our documentation lacking, unclear, or incorrect, you can help me improve it.

## Guidelines for Merge Requests

To ensure smooth collaboration and maintain code quality, please follow these guidelines when submitting a merge request:

1. **Fork the Repository**: Fork the Luisa ROS2 Robotics Project repository to your GitLab account.

2. **Create a Branch**: Create a new branch in your forked repository for your contributions.

3. **Keep Your Branch Updated**: Make sure to regularly pull the latest changes from the main branch of the upstream repository to avoid conflicts.

4. **Write Meaningful Commit Messages**: Provide clear and concise commit messages to describe the changes you made.

5. **Code Style**: Follow the coding style and conventions used in the project to ensure consistency. If unsure, check the existing code for guidance.

6. **Test Your Changes**: If applicable, test your changes thoroughly to ensure they work as intended.

7. **Documentation**: If you make changes that affect the behavior of the robot, update the relevant documentation to reflect these changes.

8. **Submit the Merge Request**: Once your changes are ready, submit a merge request to the `main` branch of the Luisa ROS2 Robotics Project repository.

## Code of Conduct

I expect all contributors to adhere to the [Code of Conduct](docs/CODE_OF_CONDUCT.md) to create a friendly and inclusive community.

## License

By contributing to the Luisa ROS2 Robotics Project, you agree that your contributions will be licensed under the terms of the [GNU Affero General Public License (AGPL) version 3.0](LICENSE).

---

Thank you for helping make the Luisa ROS2 Robotics Project better! I appreciate your contributions.

If you have any questions, feel free to reach out to [Ignacio Gaitán](mailto:igaitan@icloud.com).